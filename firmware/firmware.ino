#include <Arduino.h>
#include <Wire.h>

//// Temperature sensor
//#include <OneWire.h>
//#include <DallasTemperature.h>
//// Data wire is plugged into port 2 on the Arduino
//#define ONE_WIRE_BUS 2


uint32_t meas_counter = 0;
int32_t thresholdPPM = 450;

// """"""""""""""""""""""""""""""""""

// SCD4x **********************
const int16_t SCD_ADDRESS = 0x62;

unsigned long getDataTimer = 0;

const int cal_pin = 6;  // calibration button
unsigned long timeElapse = 0;//used to wait the sensor to stabilize before calibration

float co2, temperature, humidity;
  uint16_t calibration;
  uint8_t data[12], counter, repetition;
  uint8_t ret;
// """"""""""""""""""""""""""""""""""


void setup() {

  Serial.begin(9600);
  // wait for serial connection from PC
  // comment the following line if you'd like the output
  // without waiting for the interface being ready
  while(!Serial);

  // init I2C
  Wire.begin();

    delay(500);

  // start scd measurement in periodic mode, will update every 5 s
  Wire.beginTransmission(SCD_ADDRESS);
  Wire.write(0x21);
  Wire.write(0xb1);
  Wire.endTransmission();
  // wait for first measurement to be finished
  delay(2000);
}
void loop() {

  if (millis() - getDataTimer >= 5000){

  float co2, temperature, humidity;
  uint8_t data[12], counter;

  // send read data command
  Wire.beginTransmission(SCD_ADDRESS);
  Wire.write(0xec);
  Wire.write(0x05);
  Wire.endTransmission();

  // read measurement data: 2 bytes co2, 1 byte CRC,
  // 2 bytes T, 1 byte CRC, 2 bytes RH, 1 byte CRC,
  // 2 bytes sensor status, 1 byte CRC
  // stop reading after 12 bytes (not used)
  // other data like  ASC not included
  Wire.requestFrom(SCD_ADDRESS, 12);
  counter = 0;
  while (Wire.available()) {
    data[counter++] = Wire.read();
  }

  // floating point conversion according to datasheet
  co2 = (float)((uint16_t)data[0] << 8 | data[1]);
  // convert T in degC
  temperature = -45 + 175 * (float)((uint16_t)data[3] << 8 | data[4]) / 65536;
  // convert RH in %
  humidity = 100 * (float)((uint16_t)data[6] << 8 | data[7]) / 65536;

  Serial.print(getDataTimer);
  Serial.print(" ");
  Serial.print (co2);
  Serial.println();


  // call sensors.requestTemperatures() to issue a global temperature
  // request to all devices on the bus
//  Serial.print("Requesting temperatures...");
//  sensors.requestTemperatures(); // Send the command to get temperatures
//  Serial.println("DONE");
//  // After we got the temperatures, we can print them here.
//  // We use the function ByIndex, and as an example get the temperature from the first sensor only.
//  float tempC = sensors.getTempCByIndex(0);
//
//  // Check if reading was successful
//  if(tempC != DEVICE_DISCONNECTED_C)
//  {
//    Serial.print("Temperature for the device 1 (index 0) is: ");
//    Serial.println(tempC);
//  }
//  else
//  {
//    Serial.println("Error: Could not read temperature data");
//  }

  getDataTimer = millis();
  meas_counter++;
   }

}

