# Sensor CO2 (SCD41)

<img src="https://cdn-learn.adafruit.com/guides/cropped_images/000/003/395/medium640/SDC40_top_angle.jpg" alt="fig. 1" width="350"/>

## Descripción

Este módulo contiene un sensor de dióxido de carbono ambiente [SCD41](https://sensirion.com/products/catalog/SCD41/). El sensor
funciona mediante el [principio fotoacústico](https://www.scienceinschool.org/es/article/2019/photoacoustics-seeing-sound-es/).
El sensor envía la información mediante una interfaz I2C.

## Especificaciones
- Resolución:  ±40 ppm 
- Rango de funcionamiento:  400 - 5000 ppm 
- Tiempo de respuesta: 60s

El módulo tiene 4 terminales (pines) de conexión:
- 1. SDA: salida de datos
- 2. SCL: señal de reloj
- 4. VCC: conectar a +5V
- 4. GND: conectar a GND

## Diagrama de conexión con Arduino

<img src="img/Schematic.png" alt="fig. 1" width="450"/>

## Carga del firmware

Esta aplicación es muy sencilla. Lee los datos del sensor y los envía a la computadora para visualizarlos.

1. Descargar a su PC el [firmware](firmware/firmware.ino)
2. Abrir la aplicación [Arduino IDE](https://www.arduino.cc/en/software)

<img src="img/arduinoIDE_1.png" alt="fig. 1" width="300"/>

3. Abrir el código que descargó en el punto 1.
4. Conectar el Arduino a la computadora con el cable USB.
5. Seleccionar el puerto serie correspondiente al Arduino.

<img src="img/arduinoIDE_2.png" alt="fig. 1" width="300"/>

6. Seleccionar la placa Arduino que corresponde.

<img src="img/arduinoIDE_3.png" alt="fig. 1" width="300"/>

7. Cargar el firmware.
8. Abrir el *monitor serial* o el *serial plotter*

<img src="img/arduinoIDE_4.png" alt="fig. 1" width="300"/>

9. Asegurarse que la velocidad de comunicación está seteada en 9600 bauds

### AHORA PROBÁ RESPIRAR CERCA DEL SENSOR!!!

